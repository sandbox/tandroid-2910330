<?php

namespace Drupal\media_entity_svg\Plugin\Field\FieldFormatter;

use DOMDocument;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'inline_svg_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "inline_svg_formatter",
 *   label = @Translation("Inline SVG"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class InlineSvgFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'generate_id' => NULL,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $generate_id_value = $this->getSetting('generate_id');
    $element['generate_id'] = array(
      '#title' => t('Generate a unique HTML ID'),
      '#type' => 'checkbox',
      '#default_value' => empty($generate_id_value) ? NULL : $generate_id_value,
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $generate_id = $this->getSetting('generate_id') ? 'True' : 'False';
    $summary = [
      'Generate HTML ID: ' . $generate_id
    ];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      /** @var \Drupal\file\Entity\File $file */
      $file = $item->entity;

      $xdoc = new DomDocument;
      $xdoc->Load(file_create_url($file->getFileUri()));

      if ($this->getSetting('generate_id')) {
        $svg_el = $xdoc->getElementsByTagName('svg')->item(0);
        $svg_el->setAttribute('id', Html::getUniqueId($file->label()));
      }
      $elements[$delta] = ['#children' => $xdoc->saveXML()];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
