<?php

namespace Drupal\media_entity_svg\Plugin\MediaEntity\Type;

use Drupal\media_entity\MediaInterface;
use Drupal\media_entity\MediaTypeBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides media type plugin for SVG.
 *
 * @MediaType(
 *   id = "svg",
 *   label = @Translation("Svg"),
 *   description = @Translation("Provides business logic and metadata for local SVG.")
 * )
 */
class Svg extends MediaTypeBase {

  /**
   * {@inheritdoc}
   */
  public function providedFields() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getField(MediaInterface $media, $name) {
    $source_field = $this->configuration['svg_field'];

    // Get the file document.
    /** @var \Drupal\file\FileInterface $file */
    $file = $media->{$source_field}->entity;

    // Return the field.
    switch ($name) {
      case 'size':
        $size = $file->getSize();
        return $size ? $size : FALSE;
      case 'svg_field':
        $x=0;
        break;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\media_entity\MediaBundleInterface $bundle */
    $bundle = $form_state->getFormObject()->getEntity();
    $options = [];
    $allowed_field_types = ['file'];
    foreach ($this->entityFieldManager->getFieldDefinitions('media', $bundle->id()) as $field_name => $field) {
      if (in_array($field->getType(), $allowed_field_types) && !$field->getFieldStorageDefinition()->isBaseField()) {
        $options[$field_name] = $field->getLabel();
      }
    }

    $form['source_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field with source information'),
      '#description' => $this->t('Field on media entity that stores Document file. You can create a bundle without selecting a value for this dropdown initially. This dropdown can be populated after adding fields to the bundle.'),
      '#default_value' => empty($this->configuration['svg_field']) ? NULL : $this->configuration['svg_field'],
      '#options' => $options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function thumbnail(MediaInterface $media) {
    $source_field = $this->configuration['svg_field'];
    /** @var \Drupal\file\FileInterface $file */
    $file = $media->{$source_field}->entity;

    if ($file) {
      $thumbnail = $file->getFileUri();
    }
    else {
      $module_path = drupal_get_path('module', 'media_entity_svg');
      $thumbnail = $module_path . '/images/icons/document.png';
    }

    return $thumbnail;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultName(MediaInterface $media) {
    // The default name will be the filename of the source_field, if present.
    $source_field = $this->configuration['svg_field'];

    /** @var \Drupal\file\FileInterface $file */
    if (!empty($source_field) && ($file = $media->{$source_field}->entity)) {
      return $file->getFilename();
    }

    return parent::getDefaultName($media);
  }

}
