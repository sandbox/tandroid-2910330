<?php

namespace Drupal\media_entity_svg;

/**
 * Helper methods for handling SVG files.
 *
 * @package Drupal\media_entity_svg
 */
class SvgManager implements SvgManagerInterface {

  /**
   * Static cache of SimpleXMLElement instances.
   *
   * @var array
   */
  protected $simpleXmlElements = [];

  /**
   * Static cache of extracted icon ids.
   *
   * @var array
   */
  protected $cachedExtractedIds = [];

  /**
   * SvgManager constructor.
   */
  public function __construct() {
  }

  /**
   * Get SimpleXML from SVG file, store it in static cache to avoid redundancy.
   *
   * @param string $svg_path
   *   SVG local or distant path.
   *
   * @return \SimpleXMLElement|string
   *   XML element.
   */
  protected function getSimpleXml($svg_path) {
    // Check static cache.
    if (isset($this->simpleXmlElements[$svg_path])) {
      return $this->simpleXmlElements[$svg_path];
    }

    $simple_xml = simplexml_load_file($svg_path);
    if (!$simple_xml) {
      $this->simpleXmlElements[$svg_path] = FALSE;
    }
    else {
      $simple_xml->registerXPathNamespace('ns', 'http://www.w3.org/2000/svg');
      $this->simpleXmlElements[$svg_path] = $simple_xml;
    }

    return $this->simpleXmlElements[$svg_path];
  }

}
