<?php

namespace Drupal\media_entity_svg;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Simplifies gathering informations of SVG media types.
 *
 * @package Drupal\media_entity_icon
 */
class SvgTypeManager implements SvgTypeManagerInterface {
  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Media type manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $mediaTypeManager;

  /**
   * Static cache of sprite bundle ids.
   *
   * @var array
   */
  protected $spriteBundleIds;

  /**
   * Static cache of sprite bundles config fields.
   *
   * @var array
   */
  protected $spriteBundlesConfig;

  /**
   * SvgTypeManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $media_type_manager
   *   Media type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PluginManagerInterface $media_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mediaTypeManager = $media_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getSpriteBundleNames($reset = FALSE) {
    if ($reset || !isset($this->spriteBundleIds)) {
      $this->spriteBundleIds = [];
      $media_bundles = $this->mediaTypeManager->getDefinitions();
      foreach ($media_bundles as $id => $definition) {
        if ('Drupal\media_entity_icon\Plugin\MediaEntity\Type\SvgSprite' === $definition['class']) {
          $this->spriteBundleIds[$id] = $definition['label'];
        }
      }
    }

    return $this->spriteBundleIds;
  }

}
