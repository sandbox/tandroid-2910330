<?php

namespace Drupal\media_entity_svg;

/**
 * Provides an interface for SvgTypeManager.
 *
 * @package Drupal\media_entity_svg
 */
interface SvgTypeManagerInterface {

  /**
   * Get sprite bundle names fetched by ids.
   *
   * @param bool $reset
   *   Whether it should purge the static cache or not.
   *
   * @return array
   *   Sprite bundles names fetched by bundle ID.
   */
  public function getSpriteBundleNames($reset = FALSE);

}
